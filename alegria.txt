Y dale alegría, alegría a mi corazón 
Es lo único que te pido al menos hoy
Y dale alegría, alegría a mi corazón 
Afuera se irán la pena y el dolor 
Y ya veras, las sombras que aquí estuvieron no estarán 
Y ya, ya veras, bebamos y emborrachemos la ciudad 
Y dale alegría, alegría a mi corazón 
Es lo único que te pido al menos hoy
Y dale alegría, alegría a mi corazón 
Y que se enciendan las luces de este amor
Y ya veras, como se transforma el aire del lugar 
Y ya veras, que no necesitaremos nada mas
Y dale alegría, alegría a mi corazón 
Que ayer no tuve un buen día, por favor 
Y dale alegría, alegría a mi corazón 
Que si me das alegría estoy mejor
Y ya veras, las sombras que aquí estuvieron no estarán 
Y ya veras, que no necesitaremos nada mas
Y dale alegría, alegría a mi corazón 
Es lo único que te pido al menos hoy
Y dale alegría, alegría a mi corazón 
Afuera se irán la pena y el dolor
Y dale alegría, alegría a mi corazón